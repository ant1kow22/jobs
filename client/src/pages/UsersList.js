import React, { useEffect, useState } from "react";
import { Button, Card, CardBody, Col, Container, Spinner } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { PROFIL_ROUTE } from "../utils/consts";

function UsersList() {
    const [isLoading, setIsLoading] = useState(true);
    const [users, setUsers] = useState([]);
    const navigate = useNavigate()

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch("http://localhost:5000/api/user/get/all");
                const data = await response.json();
                setUsers(data.users);
                setIsLoading(false);
            } catch (error) {
                console.error("Error fetching user data:", error);
            }
        };

        fetchData();
    }, []);

    return (
        <Container>
            {isLoading ? (
                <Spinner animation="border" />
            ) : (
                <Col md={4} className="mt-2">
                    {users.map((user) => (
                        <Card className="mb-2" key={user.id}>
                            <CardBody>
                                <p>Id: {user.id}</p>
                                <p>Почта: {user.email}</p>
                                <p>Роль: {user.role}</p>
                                <p>Дата регистрации: {user.createdAt.split('T')[0]}</p>
                                <Button onClick={() => navigate(PROFIL_ROUTE + '/' + user.id)}>Профиль</Button>
                            </CardBody>
                        </Card>
                    ))}
                </Col>
            )}
        </Container>
    );
}

export default UsersList;