## Consolidation
---
## Как запусить ##

Склонировать репозиторий 

```
https://gitlab.com/ant1kow22/jobs.git
```

Открыть скаченные проект в VSCode

Открыть терминал -> перейти в папку server

Установить зависимости для сервера

```
cd server

npm i
```

Открыть еще одну вкладку терминала

Установить зависимости для клиента

```
cd client

npm i
```

***Нужно заранее создать БД в postgres (просто создать новую пустую базу)*** 

Дальше в папке server изменить файл .env (Нужно заменить "NAME" и "PASSWORD",
на навание ранее созданной базы и пароль о pgAdmin соответственно)

```
PORT = 5000
DB_NAME = "NAME" 
DB_USER = postgres
DB_PASSWORD = "PASSWORD"
DB_HOST = localhost
DB_PORT = 5432
SECRET_KEY = random_key_123 
```

Запусить сервер

```
server~ npm run dev
```

Зпустить клиет
```
client~ npm start
```

